package com.torf;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GameHelper {

    private Board board = new Board();
    private Board secondBoard = new Board();
    private BoardMouseClickEvent boardMouseClickEvent = new BoardMouseClickEvent();

    public GameHelper (Stage stage) {

        board.initBoard();
        board.setBoardPaint();
        secondBoard.moveBoardRight();
        secondBoard.initBoard();
        secondBoard.setBoardPaint();
        boardMouseClickEvent.setOnFirstBoardClick(board, secondBoard);

        Group groups = new Group();
        groups.getChildren().addAll(
                board.getBoardGroup(),
                secondBoard.getBoardGroup()
        );

        Scene scene = new Scene(groups);
        stage.setScene(scene);
        stage.show();
    }
}
