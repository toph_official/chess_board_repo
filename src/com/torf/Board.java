package com.torf;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

public class Board {

    private final int SQUARE_SIZE = 40;
    private final int BOARD_SIZE = 8;
    private int startX = 0;
    private int startY = 0;
    private final String BLACK_COLOR_STRING = "#2e2e2e";
    private final String WHITE_COLOR_STRING = "#cccccc";
    private Rectangle[] boardSquares = new Rectangle[BOARD_SIZE * BOARD_SIZE];
    private Group boardGroup;

    public Board () {
    }

    public void initBoard () {
        for (int i = 0; i < boardSquares.length; i++) {
            boardSquares[i] = new Rectangle();
            boardSquares[i].setWidth(SQUARE_SIZE);
            boardSquares[i].setHeight(SQUARE_SIZE);
            boardSquares[i].setX(startX + SQUARE_SIZE * (i % BOARD_SIZE));
            boardSquares[i].setY(startY + SQUARE_SIZE * (i - i % BOARD_SIZE) / BOARD_SIZE);
        }
        boardGroup = new Group(boardSquares);
    }

    public void setBoardPaint () {
        for (int i = 0; i < boardSquares.length; i++) {
            setSquarePaint(i, (((i - i % BOARD_SIZE) / BOARD_SIZE) + (i % BOARD_SIZE)) % 2 == 0 ?
                    WHITE_COLOR_STRING : BLACK_COLOR_STRING);
        }
    }

    public void setSquarePaint (int index, String hexColorString) {
        boardSquares[index].setFill(Paint.valueOf(hexColorString));
    }

    public void moveBoardRight() {
        startX = SQUARE_SIZE * BOARD_SIZE + SQUARE_SIZE * 2;
    }

    public Group getBoardGroup () {
        return boardGroup;
    }

    public void setOnClick (Board anotherBoard) {
        for (int i = 0; i < boardSquares.length; i++) {
            int finalI = i;
            boardSquares[i].setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent t) {
                     anotherBoard.getBoardSquareByIndex(finalI).setVisible(true);
                }
            });
        }
    }

    public Rectangle getBoardSquareByIndex (int index) {
        return boardSquares[index];
    }

    public int getBOARD_SIZE() {
        return BOARD_SIZE;
    }
}
