package com.torf;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class BoardMouseClickEvent {

    public BoardMouseClickEvent() {

    }

    public void setOnFirstBoardClick (Board firstBoard, Board secondBoard) {
        for (int i = 0; i < firstBoard.getBOARD_SIZE() * firstBoard.getBOARD_SIZE(); i++) {
            int finalI = i;
            firstBoard.getBoardSquareByIndex(i).setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    firstBoard.setSquarePaint(finalI, "ff0000");
                    secondBoard.setSquarePaint(finalI, "ff0000");
                }
            });
        }
    }
}
